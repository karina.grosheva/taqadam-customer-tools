# https://towardsdatascience.com/master-the-coco-dataset-for-semantic-image-segmentation-part-1-of-2-732712631047

from pycocotools.coco import COCO
import numpy as np
import skimage.io as io
import random
import os
import cv2
import requests
import shutil
from tensorflow.keras.preprocessing.image import ImageDataGenerator

### For visualizing the outputs ###
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

#%matplotlib inline


#dataDir='./COCOdataset2017'
dataType='val'
#annFile='{}/annotations/instances_{}.json'.format(dataDir,dataType)
#annFile='annotations/instances_{}.json'.format(dataType)
annFile="Car_Damage_12-6.coco.json"

# Initialize the COCO api for instance annotations
coco=COCO(annFile)

# Load the categories in a variable
catIDs = coco.getCatIds()
cats = coco.loadCats(catIDs)

print(cats)



def getClassName(classID, cats):
    for i in range(len(cats)):
        if cats[i]['id']==classID:
            return cats[i]['name']
    return "None"

#print('The class name is', getClassName(92, cats))



# Define the classes (out of the 81) which you want to see. Others will not be shown.
#filterClasses = ['laptop', 'tv', 'cell phone']
#filterClasses = ['house']
filterClasses = ['object - Aluminum can']

# Fetch class IDs only corresponding to the filterClasses
# catIds = coco.getCatIds(catNms=filterClasses) 
# Get all images containing the above Category IDs
imgIds = coco.getImgIds()

for img_id in imgIds:
    image = coco.loadImgs(ids=img_id)[0]
    r = requests.get(image['filename'], stream=True)
    if r.status_code == 200:
        with open(image['taqadam_filename'], 'wb') as f:
            r.raw.decode_content = True
            shutil.copyfileobj(r.raw, f)   
    
    image_ann_Ids = coco.getAnnIds(imgIds=img_id, iscrowd=None)
    #I = io.imread('val2017/{}'.format(img['file_name']))/255.0
    I = io.imread(image['taqadam_filename'])/255.0

    print(f"IMAGE SIZE: {image['width']} x {image['height']}")
    print(image)

    plt.axis('off')
    plt.imshow(I)
    # plt.show()

    # plt.imshow(I)
    # plt.axis('off')
    # annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
    anns = coco.loadAnns(image_ann_Ids)
    coco.showAnns(anns)
    plt.show()




# #### GENERATE A SEGMENTATION MASK ####
# #filterClasses = ['laptop', 'tv', 'cell phone']
# #filterClasses = ['house']
# filterClasses = ['parts-damage - Grill', 'parts-damage - LT Headlamp', 'parts-damage - RT Fog lamp', 'parts-damage - Windshield', 'parts-damage - Glass', 'parts-damage - LT Tail lamp', 'parts-damage - Wheel', 'parts-damage - Wheel cap', 'parts-damage - RT Tail lamp', 'parts-damage - Side skirt', 'parts-damage - Car plate'
# 'parts-damage - RT Quarter panel','parts-damage - RT Headlamp', 'parts-damage - Side rear window', 'parts-damage - RT Quarter panel']
# mask = np.zeros((img['height'],img['width']))
# print("mask size = {}x{}".format(img['height'], img['width']))

# for i in range(len(anns)):
#     className = getClassName(anns[i]['category_id'], cats)
#     print('filterClasses: ', filterClasses)
#     print('className: ', className)

#     #if className in filterClasses:
#     #    print('I\'m here!')
#     #    pixel_value = filterClasses.index(className) +1
#     #    mask = np.maximum(coco.annToMask(anns[i])*pixel_value, mask)
#     if className in filterClasses:
#         pixel_value = filterClasses.index(className) +1
#         mask = np.maximum(coco.annToMask(anns[i])*pixel_value, mask)
#     #mask = np.maximum(50, mask)

# #print('mask: ', mask)
# print(mask.shape)
# plt.imshow(mask)
# plt.show()








#### GENERATE A BINARY MASK ####
#mask = np.zeros((img['height'],img['width']))
#for i in range(len(anns)):
#    mask = np.maximum(coco.annToMask(anns[i]), mask)
#plt.imshow(mask)
#plt.show()
