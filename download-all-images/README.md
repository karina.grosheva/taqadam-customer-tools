# DOWNLOAD ALL THE IMAGES INSIDE A PROJECT

1. You must export the txt file with the list of all the original images URLs. Please consider that this export only will consider the validated images.
![export image files](images/Export.png "Export")
2. Create a folder and put the txt file inside it. The images will be downloaded inside this folder. Make sure that there is no other `.txt` file inside the folder, the script will open every txt file.

2. Before run the script you need to install python `requests` package. You can do this by running this: `pip install requests`

3. Once you have the `.txt` file in your computer run the following script as follow:

    ```python
    python download_images /path/to/directory/containing/txt/files/
    ```


4. The script will download each image and it will put them inside a folder with the same name that the txt file.

