import shutil
import requests
import sys
from os import path, makedirs, listdir

try:
    dir_path = sys.argv[1]
    file_list = [file for file in listdir(dir_path) if file.endswith(".txt")]
except FileNotFoundError:
    print("Directory not found.")
    exit(0)
except:
    print(f"USAGE: python3 {path.basename(__file__)} /path/to/directory/containing/txt/files/")
    exit(0)

for file in file_list:
    filename = file
    img_folder_name = filename.replace('.txt', '_images')
    img_file = open(path.join(dir_path, filename), 'r')
    output_dir = path.join(dir_path, img_folder_name)

    if not path.exists(output_dir):
        makedirs(output_dir, exist_ok=False)

    lines = img_file.readlines()
    total_images = len(lines)
    for index, line in enumerate(lines, start=1):
        result = requests.get(line, stream=True)
        img_filename = line.split('?')[0].split('/')[-1]
        output_path = f"{output_dir}/{img_filename}"

        with open(output_path, 'wb') as out_file:
            shutil.copyfileobj(result.raw, out_file)

        progress = round((index/total_images)*100, 1)
        end = '\r' if index != total_images else '\n'
        print(f"    PROGRESS: {progress} %                                 ", end=end)