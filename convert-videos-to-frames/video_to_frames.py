import cv2
import sys
from pathlib import Path
import os

try:
    video_list = list(Path(sys.argv[1]).rglob('*.mp4')) \
               + list(Path(sys.argv[1]).rglob('*.avi')) \
               + list(Path(sys.argv[1]).rglob('*.mov')) \
               + list(Path(sys.argv[1]).rglob('*.MOV')) \
               + list(Path(sys.argv[1]).rglob('*.m4v')) \
               + list(Path(sys.argv[1]).rglob('*.MP4')) \

    if len(sys.argv) == 3:
        frame_rate = float(sys.argv[2])
except:
    print("USAGE: python3 video_to_frames.py /path/to/directory/containing/video/files/ frame_rate(mili-sec)")
    exit(0)

def getFrame(msec, path, filename):
    vidcap.set(cv2.CAP_PROP_POS_MSEC, msec)
    hasFrames, image = vidcap.read()
    if hasFrames:
        cv2.imwrite(("video_images/" + path.stem + "/"+ str(filename) + ".jpg"), image)
    return hasFrames

global_count = 0
for video in video_list:
    vidcap = cv2.VideoCapture(str(video))
    fps = vidcap.get(cv2.CAP_PROP_FPS)  # OpenCV2 version 2 used "CV_CAP_PROP_FPS"
    frame_count = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    duration = frame_count/fps

    if len(sys.argv) == 3:
        number_of_images = duration/(frame_rate/1000)
    else:
        frame_rate = 1000/fps
        number_of_images = frame_count

    print("video = "+str(video))
    print('fps = ' + str(fps))
    print('number of frames = ' + str(frame_count))
    print('duration (S) = ' + str(duration))
    print('number of images = ' + str(number_of_images))

    msec = 0
    count = 0
    try:
        os.makedirs(os.path.join(sys.argv[1], "video_images/", video.stem))
    except FileExistsError:
        pass

    print(f"Started working on video {video_list.index(video) + 1} from {len(video_list)}")
    filename = 'frame'+str(count).zfill(len(str(int(number_of_images))))
    success = getFrame(msec, video, filename)

    while success:
        count += 1
        msec += frame_rate
        filename = 'frame'+str(count).zfill(len(str(int(number_of_images))))
        if number_of_images < count:
            success = False
        else: 
            success = getFrame(int(msec), video, filename)
    global_count += count

print("finished successfully!")
print(f"created {global_count} new images")
