### To create a project from a video:

- Create and activate virtual environment and install requirements.
  
        virtualenv env

        source env/bin/activate

        pip install -r requirements.txt


- To be more organized, create a "video" folder in the same path where is video_to_frame.py.
- Copy the videos to convert in the folder created. 

The structure:

      -env
      -video
          --video_1.mp4
          --video_2.mp4
      -video_to_frames.py
      -requirements.txt

- Execute script "video_to_frames.py", from the root of this script as follows:

      python3 video_to_frames.py . frame_rate(milliseconds)

- Example 1: frame every 300 milliseconds:
  
        python3 video_to_frames.py . 300

- Example 2: 1 sec = 1000 msec. If we want 5 fps.
  
        python3 video_to_frames.py . 200

- Images will be saved inside ./video_images/video_1, ./video_images/video_2, etc.

- If we want to convert more video files, first remove the already converted videos and images from the folder. 

### Considerations to correctly upload the images to platform.taqadam.io:

- For each video that we convert we will have a folder with all its frames.
- Go inside a folder:
        
        cd video_images/video_1/

- Compress frames

        zip name_zip.zip ./*

- Do not compress an entire folder. In the compressed file there should be only the single images:

Correct:

        - zip
            - image_result_1.jpg
            - image_result_2.jpg
            - image_result_3.jpg
            - image_result_4.jpg
            - etc

Incorrect:

        - zip
            - video_1/
                - image_result_1.jpg
                - image_result_2.jpg
                - image_result_3.jpg
                - image_result_4.jpg
                - etc